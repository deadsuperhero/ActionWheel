// Main header script - this will be included into every script in
// the game (local and global). Do not place functions here; rather,
// place import definitions and #define names here to be used by all
// scripts.
 import function contextLook();
 import function contextTalk();
 import function contextInteract();
 import function checkController();
 import function startController();
 import function setIcons();
 import function trackAxis();
 import function checkButtons();
 import function Message(String messageText);
